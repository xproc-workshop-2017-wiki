As somebody has to kick off the conversation: Here are my thoughts about the workshop in February:
* I'm very interested in continuing the process we started in Amsterdam.
* After the more general discussion we had there and based on the positions we have developed there, I think we are now ready to do some detailed work on XProc 1.1 itself. As an aim for the workshop I propose to have the next version of the editor's drafts (the general one and the one concerning the step library). By the way: We seem to have agreed tacitly the version number for "next" is 1.1. I do not think that this does justice to the efforts of the former working group.
* Given this goal and looking back at the progress we made in Amsterdam, I would suggest a workshop of two days. We could have different agendas for each day, so that it is also possible to attend just one day. Alternatively we could work in two phases, discussing first day's results in a larger group on the second day.
* As XML Prague will happen on February 9–11, I would suggest to have the workshop on February 7-8. If we can't establish a critical mass for this dates, may be we could use the conference's workshop day, so meet on February 8-9.
* Concerning the place: If I remember correctly, there was an idea to meet in Leipzig hosted by Gerrit's company, so we do not have to look (and pay) for a venue in Prague. I have no idea, whether this is still a good plan and if it is still o.k. with Gerrit. May be it is not so convenient for most of us to go to Leipzig first and then to proceed to Prague. (For those not familiar with the connections: Leipzig to Prague is about three hours by train and there is a train going every two hours.) For me Leipzig or Prague would be fine, but I think it is up to the people not living in Germany to decide, what would be a good place. I think we should be able to find a venue in Prague: May be we could get a room at the university with Jirka's help?
* I think it would be very helpful, if we could do some work in advance. I am not quite sure, which topics are the most important or the most difficult to discuss. I would volunteer do some work on non-XML documents in XProc 1.1., if this is of any interest. If somebody wants to join me, (s)he would be more than welcome. Of course I am also open to other assignments.

I am looking forward to do some work with all of you!

Greetings from Germany,

Achim

---------------------------------------------------------

Not sure this is the place to add my thoughts... 
* I concur on the "two" days in order to get some work done
* I kinda like Gerrit's offer to have the two days in Leipzig, have never been there and I heard they have "Lerche" :-)
* Not sure it would make a difference for peoples opinion on the location but...
Leipzig - Prague is a three hours drive, I tend to drive to Prague next year as I usually do and 
I can offer 4 seats comfortably in the car for the travel Leipzig - Prague on the evening of February 8th
* Happy to take up some reading / testing /discussing work as a preparation of the workshop
Best regards
Geert

---------------------------------------------------------

We stand by our offer to host the meeting in Leipzig. For visitors from outside the Schengen area, they only need a single visa for Germany and Czechia. There’s a Ryanair connection to Leipzig from Stansted (currently priced at € 24, conveniently departing at 6pm on Feb. 6), but otherwise the abundant international connections to Berlin (1 hr by train) or Frankfurt (3 hrs by train) will do. From Leipzig to Prague will be a mere 2.5 hrs ride if the [D8 motorway](https://en.wikipedia.org/wiki/D8_motorway_(Czech_Republic)) is finally opened (and if you can restrain yourselves from nibbling the 200 [Leipzig larks](https://en.wikipedia.org/wiki/Leipziger_Lerche) that will travel with us). Since some of us will go by train, there will be some places left in my car, in Geerts car, and maybe also in Philipp’s car. Otherwise, we can rent a van or some can go by bus or train. Leipzig has a night life that is almost on par with Prague’s, and there is always music in the air in the city of Bach and Mendelssohn. For example, on [Feb. 7](https://www.gewandhausorchester.de/en/schedule/), my friend Wolfram Strasser, a Gewandhaus Orchestra horn player, is giving an “After Work Concert” together with 11 other horn players, as it seems. After that, we can have an “after after work concert beer” at [Moritzbastei](https://en.wikipedia.org/wiki/Moritzbastei), a student club that Angela Merkel helped unearth when she was a physics student in Leipzig. Accommodation is affordable in Leipzig, and at le-tex, we have meeting rooms, projectors, wifi, decent coffee, … at your disposal for free. Also Leipziger Lerchen with the coffee, if that is what it takes to lure you in. So I’m suggesting that we hold the meeting on Feb. 7 and 8 in Leipzig.

Gerrit (2016-11-19)

---------------------------------------------------------
Sounds like a very good plan to me. Thanks Gerrit!
So: Can we agree on Leipzig as a venue and on Feb. 7 and 8?

Achim (2016-11-20)

----------------------------------------------------------
From a mail on xproc-dev by Jirka Kosek:

I see that workshop is going to be in Leipzig. I highly recommend to
visit this city. However if for some reason you decide that Prague will
be better location I can arrange room for free at university. Just let
me know as soon as possible if you would like to use this opportunity.

Also I hope you can create 20 minute summary what you have achieved and
what will be next directions for XProc that will be presented during the
conference. Deal? :-)

Cheers,
Jirka
----------------------
Update 2016-12-16: It will be in Prague (on Feb. 7/8) since it was more convenient 
for some participants and because Jirka managed to secure a meeting room
at no charge in Prague. See [Venue](https://github.com/xproc/Workshop-2017-02/wiki/Venue)
for details.

---------------
Topic notice, Henry S. Thompson, 2017-01-17:  I'll be there, and propose we spend at least some f2f time talking about the flow model/abstract API/virtual machine that we might use to try to talk about what pipelines in V2 actually _are_, as opposed to how they are notated.  I know Norm has some new ideas about this, and they are surprisingly close to what I had hoped to talk about in September but never got to...