Meeting will take place at the University of Economics in Prague -- the [same venue](http://www.xmlprague.cz/venue/) as the XML Prague conference. 

Meeting will be in the room NB 468. This room is in the same building (so called "Nová budova"/"New Building") as main conference sessions. There are lifts and stairs in front of the cloak room. Use lift to go to the floor 4. Then use corridor on your right side, room is almost at the end of the corridor.

Projector, WiFi connectivity, coffee, tea and small refreshments will be provided. Also air, electricity and tap water will be there.

There are a lot of navigation tables and pointers at the campus so keep trying if you get lost. 


