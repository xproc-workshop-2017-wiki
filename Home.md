Welcome to the Workshop-2017-02 wiki!

These pages exist for proposing topics, an agenda, etc. for the XProc workshop colocated with XML Prague on 7 and 8 February, 2017.

Please create pages (see list at right) as appropriate.
