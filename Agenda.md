## Proposed daily schedule

```text
09:00 - Gather and setup laptops, etc.
09:30 - Workshop begins
13:00 - Lunch
17:00 - Wrap up for the day
```

The conference may not be able to provide coffee or lunch, so we'll plan to forage for ourselves.

## Proposed agenda

* Introductions
* Review of the agenda
  * Proposed: Review of the test suite mechanics
* Brief review of the previous workshop minutes
* Review of the current working drafts
* Review of open issues
* Proposed list of issues that warrant face-to-face time:
  * How to improve debugging
    * [issue 18 ](https://github.com/xproc/1.1-specification/issues/18)
    * [@cx:message](https://github.com/xproc/1.1-specification/issues/29) 
* Next steps
* Adjourned
